class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.string :nome
      t.text :descricao
      t.date :date
      t.string :desenvolvedora
      t.string :imagem
      t.string :video
      t.string :genero

      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  get 'games/home'

  get 'games/about'

  get 'games/catalogo'

  root 'games#home'

  resources :games
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
